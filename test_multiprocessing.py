# based on https://medium.com/mindful-engineering/multithreading-multiprocessing-in-python3-f6314ab5e23f
import multiprocessing
import time


def calc_square(numbers, q):
    print("Calculate square numbers: ")
    for i in numbers:
        time.sleep(1)  # artificial time-delay
        r = str(i * i)
        s = f'square of {i}: {r}'
        print(s)
        q.put(s)


def calc_cube(numbers, q):
    print("Calculate cube numbers: ")
    for i in numbers:
        time.sleep(1)
        r = str(i * i*i)
        s = f'cube of {i}: {r}'
        print(s)
        q.put(s)


if __name__ == "__main__":
    arr = [2, 3, 8, 9, 11, 13]
    q = multiprocessing.Queue()
    p1 = multiprocessing.Process(target=calc_square, args=(arr, q))
    p2 = multiprocessing.Process(target=calc_cube, args=(arr, q))
    # creating two Process here p1 & p2
    p1.start()
    p2.start()
    # starting Processes here parallel by using start function.
    p1.join()
    # this join() will wait until the calc_square() function is finished.
    p2.join()
    # this join() will wait unit the calc_cube() function is finished.

    print("--- Now From queue!:")
    while q.empty() is False:
        print(q.get())

    print("Successes!")
