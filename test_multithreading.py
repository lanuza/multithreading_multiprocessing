# based on https://medium.com/mindful-engineering/multithreading-multiprocessing-in-python3-f6314ab5e23f
import queue
import threading
import time


def calc_square(numbers, q):
    print("Calculate square numbers: ")
    for i in numbers:
        time.sleep(1)  # artificial time-delay
        r = str(i * i)
        s = f'square of {i}: {r}'
        print(s)
        q.put(s)


def calc_cube(numbers, q):
    print("Calculate cube numbers: ")
    for i in numbers:
        time.sleep(1)
        r = str(i * i * i)
        s = f'cube of {i}: {r}'
        print(s)
        q.put(s)


if __name__ == "__main__":
    arr = [2, 3, 8, 9, 11, 13]
    q = queue.Queue()
    t1 = threading.Thread(target=calc_square, args=(arr, q))
    t2 = threading.Thread(target=calc_cube, args=(arr, q))
    # creating two threads here t1 & t2
    t1.start()
    t2.start()
    # starting threads here parallel by using start function.
    t1.join()
    # this join() will wait until the cal_square() function is finished.
    t2.join()
    # this join() will wait unit the cal_cube() function is finished.

    print("--- Now From queue!:")
    while q.empty() is False:
        print(q.get())

    print("Successes!")
